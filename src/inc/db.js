const sqlite3 = require('sqlite3').verbose();
let db = new sqlite3.Database('app.sqlite3');
const md5 = require('md5');
let conn;

module.exports.getDbConnection = function () {
    return new Promise((resolve, reject) => {
        //const dbPath = path.resolve(__dirname, 'cfs.db');
        conn = new sqlite.Database(db,//':memory:' ./db/chinook.db
            (err) => {
                if (err) {
                    reject();
                } else {
                    resolve();
                }
            });
    });
}

module.exports.waitGetDbConnection = async function () {
    try {
        await getDbConnection();
        return true;
    } catch (err) {
        return false;
    }
}

module.exports.getCookie = function (cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

module.exports.isLoggedIn = function () {
    let loggedIn = this.getCookie("loggedin");
    return (loggedIn != "" && loggedIn == true);
}

module.exports.loadPage = function (page, storage) {
    $('#page-holder').html("");
    $("#loader").hide();
    //return
    $.ajax({
        url: page,
        crossDomain: true,
        context: document.body
    }).done(function (html) {
        $("#loader").hide();
        $('#page-holder').html(html);
        setTimeout(function () {
            $('.card').removeClass('card-hidden');
        }, 300);
        if(page === "inc/login_page.html"){
            storage.get('userDetails', function(error, data) {
                $('#email').val(data.email)
            });
        }
        if(page === "inc/profile_page.html"){
            storage.get('userDetails', function(error, data) {
                $('#email').val(data.email)
                $('#username').val(data.username)
            });
        }
    }).fail(function () {
        //alert("error");
    })
}



module.exports.setCookie = function (cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}


module.exports.createDatabase = async function (email, username, password) {//sqltime TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
    db.serialize(() => {
        let email = "niyi@synnods.com"
        let username = "niyious"
        let password = md5("salaudeen")
        const stmt = db.run("INSERT INTO users (email, username, user_pass) VALUES (?, ?, ?)", [email, username, password], function (err) {
            if (err) {
                console.log(err.message);
            } else {
                console.log(`A row has been inserted with rowid ${this.lastID}`);
            }
        });
        db.close();
    });
}

module.exports.createUser = async function (email, username, password) {//sqltime TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
    db = new sqlite3.Database('app.sqlite3');
    let sql = `INSERT INTO users (email, username, user_pass) VALUES (?, ?, ?)`;
    return new Promise((resolve, reject) => {
        db.get(sql,  [email, username, md5(password)], (err, row) => {
            if (err) {
                //console.log('Error running sql ' + sql)
                reject(err)
            } else {
                resolve({status:true})
                //resolve({ id: this.lastID })
            }
        })
    })
    /*db.serialize(() => {
        let email = "niyi@synnods.com"
        let username = "niyious"
        let password = md5("salaudeen")
        const stmt = db.run("INSERT INTO users (email, username, user_pass) VALUES (?, ?, ?)", [email, username, password], function (err) {
            if (err) {
                console.log(err.message);
            } else {
                console.log(`A row has been inserted with rowid ${this.lastID}`);
            }
        });
        db.close();
    });*/
}

module.exports.selectUser = async function (name_email, password) {
    db = new sqlite3.Database('app.sqlite3');

    //run(sql, params = []) {
    let sql = `SELECT * FROM users WHERE (email = ? OR username = ?) AND user_pass = ? LIMIT 1`;
        return new Promise((resolve, reject) => {
            db.get(sql, [name_email, name_email, md5(password)], (err, row) => {
                if (err) {
                    console.log('Error running sql ' + sql)
                    console.log(err)
                    reject(err)
                } else {
                    resolve(row)
                    //resolve({ id: this.lastID })
                }
            })
        })
}

String.prototype.alphaNumeric = function () {
    return (this.replace(/[^a-z0-9]/gi, '') && this.length > 8);
}


module.exports.validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function sendAsyncMessage(ipcRender, channel, message) {
    ipcRenderer.send(channel, message);
    //console.log('async sending...');
}

function sendSyncMessage(ipcRender, channel, message) {
    ipcRenderer.sendSync(channel, message);
    console.log('async sending...');
}
