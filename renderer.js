'use strict'
const {electron} = require('electron');
const {ipcRenderer} = require('electron')
const path = require('path');
const toolKit = require('./inc/db.js');

const os = require('os');
const storage = require('electron-json-storage');
storage.setDataPath(os.tmpdir());
//const service = require('../functions/functions.js');

toolKit.createDatabase();
//toolKit.selectUser();

$(document).ready(function () {
    let page = "";
    storage.get('userDetails', function (error, data) {
        if (error || typeof data.email === "undefined") {
            /*alert(0);*/
            toolKit.setCookie("loggedIn", false, 2)
            toolKit.loadPage("inc/login_page.html", storage)
            return
        } else {
            page = "inc/profile_page.html";// "inc/login_page.html";
            setTimeout(function () {
                toolKit.loadPage(page, storage);
            }, 300);

        }
    });
});//loadPage('inc/signup_page.html')

function doLougot() {
    storage.remove('userDetails', function (error) {
        if (!error) {
            toolKit.setCookie("loggedIn", false, 2)
            toolKit.loadPage("inc/login_page.html", storage)
        }
    });
}

function doSignup() {
    let email = $('#email').val()
    let password = $('#password').val()
    let password1 = $('#password1').val()
    let username = $('#password').val()
    if (toolKit.validateEmail(email) && email.length > 8 && password.length > 8 && password == password1) {
        toolKit.setCookie("email", email, 1)
        //let result = toolKit.selectUser(email, password)
        toolKit.createUser(email, password, username).then(function (res) {
            if (res) {
                if (res.status == true)
                    $("#success_div").html("Successful signup").show()
                $("#error_div").hide()
                let page = "inc/login_page.html";// "inc/login_page.html";
                setTimeout(function () {
                    toolKit.loadPage(page, storage);
                }, 500);
            } else {
                $("#success_div").hide()
                $("#error_div").html("Signup could be completed! Please retry").show()
            }
        });
    } else {
        $("#success_div").hide()
        $("#error_div").html("Error! Valid minimal lenght of inout fiels is 8 character with valid email").show()
    }
}

function doLogin() {
    let email = $('#email').val();
    let password = $('#password').val();
    if (email.length > 8 && password.length > 8) {
        toolKit.setCookie("email", email, 1)
        //let result = toolKit.selectUser(email, password)
        toolKit.selectUser(email, password).then(function (res) {
            if (res) {
                storage.set('userDetails', res, function (error) {
                    if (!error) {
                        $("#success_div").html("Successful login...").show()
                        toolKit.loadPage("inc/profile_page.html", storage)
                        toolKit.setCookie("loggedIn", true, 2)
                    } else {
                        $("#success_div").hide()
                        $("#error_div").html("Error! Invalid login details. Please retry").show()
                    }
                });
                //console.log(res)
            } else {
                $("#success_div").hide()
                $("#error_div").html("Error! Invalid login details. Please retry").show()
            }
        });
    } else {
        $("#success_div").hide()
        $("#error_div").html("Error! Invalid login details. Please retry").show()
    }
}

function loadPage(page) {
    toolKit.loadPage(page, storage)
}
